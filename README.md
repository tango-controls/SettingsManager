[![logo](http://www.tango-controls.org/static/tango/img/logo_tangocontrols.png)](http://www.tango-controls.org)

# SettingsManager
A tool based on files to standardize and centralize the settings management for the control system. It needs JTango.jar to compile or to run.

## SettingsManager Releases / Java versions


### SettingsManager latest release compatible with Java 17 or higher

Please note that the SettingsManager java classes starting from release 5.0 and onwards are compiled with jdk-17. So you absolutely need a JVM version 17 or higher
to execute the SettingsManager latest releases in Maven Central repository.


[![Download](https://img.shields.io/badge/SettingsManager-v5.1-brightgreen) ](https://repo1.maven.org/maven2/org/tango-controls/ds/SettingsManager/5.1/)
<code>&emsp;</code>



### SettingsManager old release compatible with Java 8 or higher

This release is provided for those who **cannot** use JVM 17 or higher at run time and they need to run the SettingsManager classes using an older JVM.

[![Download](https://img.shields.io/badge/SettingsManager-v4.4-orange) ](https://repo1.maven.org/maven2/org/tango-controls/ds/SettingsManager/4.4/)
<code>&emsp;</code>


